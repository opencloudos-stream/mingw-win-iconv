%{?mingw_package_header}

%global pkgname win-iconv

Name:          mingw-%{pkgname}
Version:       0.0.8
Release:    1%{?dist}
Summary:       Iconv implementation using Win32 API

License:       LicenseRef-Fedora-Public-Domain
URL:           https://github.com/win-iconv/win-iconv
Source0:       https://github.com/win-iconv/win-iconv/archive/v%{version}/%{pkgname}-%{version}.tar.gz
BuildArch:     noarch

BuildRequires: make
BuildRequires: mingw32-filesystem >= 95
BuildRequires: mingw32-gcc
BuildRequires: mingw32-gcc-c++
BuildRequires: mingw32-binutils

BuildRequires: mingw64-filesystem >= 95
BuildRequires: mingw64-gcc
BuildRequires: mingw64-gcc-c++
BuildRequires: mingw64-binutils

BuildRequires: cmake >= 2.8.0
BuildRequires: dos2unix


%description
MinGW Windows Iconv library


%{?mingw_debug_package}


%package -n mingw32-win-iconv
Summary:       MinGW Windows Iconv library
Provides:      mingw32-iconv = 1.12-14

%description -n mingw32-win-iconv
MinGW Windows cross compiled Iconv library.

%package -n mingw32-win-iconv-static
Summary:       Static version of the MinGW Windows Iconv library
Requires:      mingw32-win-iconv = %{version}-%{release}
Provides:      mingw32-iconv-static = 1.12-14

%description -n mingw32-win-iconv-static
Static version of the MinGW Windows Iconv library.

%package -n mingw64-win-iconv
Summary:       MinGW Windows Iconv library
Provides:      mingw64-iconv = 1.13.1-2%{?dist}

%description -n mingw64-win-iconv
MinGW Windows Iconv library

%package -n mingw64-win-iconv-static
Summary:       Static version of the MinGW Windows Iconv library
Requires:      mingw64-win-iconv = %{version}-%{release}
Provides:      mingw64-iconv-static = 1.13.1-2%{?dist}

%description -n mingw64-win-iconv-static
Static version of the MinGW Windows Iconv library.


%prep
%autosetup -n %{pkgname}-%{version}

dos2unix readme.txt
dos2unix ChangeLog
chmod -x readme.txt
chmod -x ChangeLog


%build
%mingw_cmake -DBUILD_STATIC=1


%install
%mingw_make_install DESTDIR=%{buildroot}

rm -rf %{buildroot}%{mingw32_bindir}/*.exe
rm -rf %{buildroot}%{mingw64_bindir}/*.exe



%files -n mingw32-win-iconv
%doc ChangeLog readme.txt
%{mingw32_bindir}/iconv.dll
%{mingw32_includedir}/iconv.h
%{mingw32_libdir}/libiconv.dll.a

%files -n mingw32-win-iconv-static
%{mingw32_libdir}/libiconv.a

%files -n mingw64-win-iconv
%doc ChangeLog readme.txt
%{mingw64_bindir}/iconv.dll
%{mingw64_includedir}/iconv.h
%{mingw64_libdir}/libiconv.dll.a

%files -n mingw64-win-iconv-static
%{mingw64_libdir}/libiconv.a


%changelog
* Fri Apr 12 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.0.8-1
- initial build
